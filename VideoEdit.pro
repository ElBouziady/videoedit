#-------------------------------------------------
#
# Project created by QtCreator 2013-10-27T15:35:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VideoEdit
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp
   # ../../../../../../Qt/SiliconSoft/opencv_tools.cpp

HEADERS  += mainwindow.h
    #../../../../../../Qt/SiliconSoft/opencv_tools.h

FORMS    += mainwindow.ui


LIBS += `pkg-config --libs opencv`
LIBS+= -L"/usr/local/lib/"  -lopencv_highgui
INCLUDEPATH += '/usr/local/include'

RESOURCES += \
    icon.qrc

