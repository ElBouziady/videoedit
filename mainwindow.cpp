﻿#include "mainwindow.h"
#include "ui_mainwindow.h"


QString NumToString(int num,int sh);
void rotate(cv::Mat& src, double angle, cv::Mat& dst);
void RotateInv(cv::Mat& src, cv::Mat& dst);
void RotateRigth(cv::Mat& src, cv::Mat& dst);
void RotateLeft(cv::Mat& src, cv::Mat& dst);
void ConvMono(cv::Mat& src, cv::Mat& dst);


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    index_image_freq_=0;
    image_index_ =0;
    video_index_=0;
    writer = NULL;
    cvReleaseVideoWriter(&writer);
    recording_video_=false;
    video_cap_= new VideoCapture;
    video_writer_= new VideoWriter;
    ui->setupUi(this);
   // ui->dock_control->setFixedHeight(150);

    /**
     * @brief changement du repertoir
     */
    connect(ui->button_choose_deirectory_in,SIGNAL(clicked()),this,SLOT(ChangeDirectoryIn()));
    connect(ui->button_choose_deirectory_out,SIGNAL(clicked()),this,SLOT(ChangeDirectoryOut()));
    /**
     * @brief bouton cotrol flux video
     */
    connect(ui->button_play,SIGNAL(clicked()),this,SLOT(Play()));
    connect(ui->button_pause,SIGNAL(clicked()),this,SLOT(Pause()));
    connect(ui->button_stop,SIGNAL(clicked()),this,SLOT(Stop()));
    connect(ui->button_recorde,SIGNAL(clicked()),this,SLOT(Recording()));
    connect(ui->button_title_,SIGNAL(clicked()),this,SLOT(ChangeTitle()));

    connect(&timer_,SIGNAL(timeout()),this,SLOT(StepImg()));
    connect(ui->spin_fps_in,SIGNAL(valueChanged(int)),this,SLOT(FpsIn()));

    last_value=0;
    new_value=0;
}

void MainWindow::StepImg()
{
    Mat mat_original;
    int nbr_channels=1;
    // Lecture de la nouvelle frame "Image"
    //*******************************************************************************************************
    if(ui->radio_in_video->isChecked())
    {
        if(!video_cap_->read(mat_original))
        {
            if(recording_video_)
            {
                ui->zone_log_->append("\nTotal frame:"+QString::number(total_frame));
                recording_video_=false;
                video_index_++;
            }
            Stop();
            mat_original.release();
            return;
        }
    }
    else
    {
        if(image_ind_>=image_path_.length())
        {

            ui->zone_log_->append("\n indice out");
            if(recording_video_)
            {
                ui->zone_log_->append("\nTotal frame:"+QString::number(total_frame));
                recording_video_=false;
                video_index_++;
            }
            Stop();
            mat_original.release();
            return;
        }
        mat_original = imread(image_path_.at(image_ind_).absoluteFilePath().toStdString());
        image_ind_++;
    }


    if(mat_original.empty())
    {
        ui->zone_log_->append("\n  image is empty");
        mat_original.release();
        return;
    }

    //*******************************************************************************************************
    // un test de perte lecture

//    new_value = (int)mat_original.data[0];
//    if(new_value!=last_value+1 && new_value!=0)
//    {
//        std::cout<<std::endl<<"value reading: "<<new_value;
//        std::cout<<" ----------------------Error--------------( "<<last_value+1<<" )";
//        std::cout<<std::endl;
//    }
//    last_value = new_value;

    //*******************************************************************************************************

    //mat_image_=mat_original;
    if(mat_original.channels()==3)
        cvtColor(mat_original,mat_image_,CV_RGB2GRAY);
    else
        mat_original.copyTo(mat_image_);

    if(ui->check_gayer_to_rgb_->isChecked())
    {
        cvtColor(mat_image_,mat_image_,CV_BayerGB2BGR);

        nbr_channels=3;
    }






    ////////////////////////////////////////////////////////////////////////////////////////////
    //Orientation
    ////////////////////////////////////////////////////////////////////////////////////////////
    Mat mat_visual;

    if(ui->radio_orientation_90->isChecked())
    {
        mat_visual.create(mat_image_.cols,mat_image_.rows,mat_image_.type());
        RotateRigth(mat_image_,mat_visual);
    }
    else if(ui->radio_orientation_i90->isChecked())
    {
        mat_visual.create(mat_image_.cols,mat_image_.rows,mat_image_.type());
        RotateLeft(mat_image_,mat_visual);
    }
    else if(ui->radio_orientation_180->isChecked())
    {
        mat_visual.create(mat_image_.rows,mat_image_.cols,mat_image_.type());
        RotateInv(mat_image_,mat_visual);
    }
    else
        mat_visual = mat_image_;
//imshow("Mask", mat_visual);
//waitKey(24);
    ////////////////////////////////////////////////////////////////////////////////////////////
    //Titre
    ////////////////////////////////////////////////////////////////////////////////////////////

    if(ui->radio_title_avtive_->isChecked())
    {
 //       mat_visual=OpencvTools::FusionMatT(mat_title_,mat_visual);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////
    //Visualisation
    ////////////////////////////////////////////////////////////////////////////////////////////



    if(mat_visual.cols>mat_visual.rows)
        cv::resize(mat_visual,mat_display_,Size(550,mat_visual.rows*550/mat_visual.cols),0,0,INTER_CUBIC);
    else
        cv::resize(mat_visual,mat_display_,Size(mat_visual.cols*550/mat_visual.rows,550),0,0,INTER_CUBIC);

     QImage *qimg_original;
    if(mat_display_.channels()==1)
        qimg_original= new QImage((uchar*)mat_display_.data,mat_display_.cols, mat_display_.rows,mat_display_.step,QImage::Format_Indexed8);
    else
        qimg_original= new QImage((uchar*)mat_display_.data,mat_display_.cols, mat_display_.rows,mat_display_.step,QImage::Format_RGB888);

    ui->zone_video_->setPixmap(QPixmap::fromImage(*qimg_original));

    /////////////////////////////////////////////////////////////////////////////////////////////
    //Enregistrement
    /////////////////////////////////////////////////////////////////////////////////////////////

    if(ui->button_recorde->isChecked())
    {
        if(index_image_freq_ >= ui->spinBox_freq_image_->value())
        {
            index_image_freq_=1;
            /////////////////////////////////////////////////////////////////////////////////////////
            //Enregistrement video
            /////////////////////////////////////////////////////////////////////////////////////////
            if(ui->radio_out_video->isChecked())
            {
                //*******************************************************************************************************
                if(!recording_video_)
                {
                    QString name;

                    name =ui->line_edit_directory_out->text()+"\\video_"+ QString::number(video_index_);

                    Size size;
                    size.width = mat_visual.cols ;
                    size.height = mat_visual.rows ;

                    int niveau_comression = 1;

                    if(!ui->check_compressed_->isChecked()){
                        name += ".avi";
                        video_writer_->open(name.toStdString().c_str(),0,ui->spin_fps_out->value(),size,ui->check_gayer_to_rgb_->isChecked());

                    }else if(niveau_comression == 0){
                        name += ".avi";
                        video_writer_->open(name.toStdString().c_str(),CV_FOURCC('M','J','P','G'),ui->spin_fps_out->value(),size,ui->check_gayer_to_rgb_->isChecked());

                    } else if(niveau_comression == 1){
                        name += ".mp4";
                        video_writer_->open(name.toStdString().c_str(),CV_FOURCC('M','J','P','G'),ui->spin_fps_out->value(),size,ui->check_gayer_to_rgb_->isChecked());

                    }else if(niveau_comression == 2){
                        name += ".mp4";
                        video_writer_->open(name.toStdString().c_str(),CV_FOURCC('P','I','M','1'),ui->spin_fps_out->value(),size,ui->check_gayer_to_rgb_->isChecked());

                    } else{
                        name += ".mp4";
                        video_writer_->open(name.toStdString().c_str(),CV_FOURCC('D','I','V','X'),ui->spin_fps_out->value(),size,ui->check_gayer_to_rgb_->isChecked());
                    }

                    frame_max_= (4*1024*1024)/((mat_visual.cols*mat_visual.rows*mat_visual.channels())/1024);

                    if(ui->check_compressed_->isChecked())
                        frame_max_ *= 10;
                    ui->zone_log_->append("frame max: "+QString::number(frame_max_));
                    total_frame=1;

                    if(video_writer_->isOpened())
                    {
                        video_index_++;
                        ui->zone_log_->append("Creation du video: "+name+" \n->Reussi...");
                    }
                    else
                        ui->zone_log_->append("Creation du video: "+name+" \n->Echec...");
                    recording_video_=true;
                }
                else
                {

                    ui->zone_log_->append(". ");
                    video_writer_->write(mat_visual);
                    //cvWriteFrame(writer,im_opencv);
                    total_frame++;


                }
                if(total_frame>frame_max_)
                {
                    video_writer_->release();
                    recording_video_ = false;
                    video_index_++;
                }


                //cvReleaseImage(&im_opencv);
            }
            ////////////////////////////////////////////////////////////////////////////////////////
            //Enregistrement Images
            ////////////////////////////////////////////////////////////////////////////////////////
            else
            {
                int number = ui->spinBox_num_->value() *10000;
                number+=image_index_;
                QString name_file = ui->line_edit_directory_out->text()+
                        "\\image"+NumToString(number,6)+".bmp";

                //*******************************************************************************************************
                // un test de perte lecture

                new_value = (int)mat_visual.data[0];
                if(new_value!=last_value && new_value!=0)
                {
                    std::cout<<std::endl<<"value recording image: "<<new_value;
                    std::cout<<" ----------------------Error--------------( "<<last_value<<" )";
                    std::cout<<std::endl;
                }


                //*******************************************************************************************************

                imwrite(name_file.toStdString(),mat_visual);
                ui->zone_log_->append("Creation:\n"+name_file);
                image_index_++;

            }

        }
        else
        {
            index_image_freq_++;
        }


    }
    else if(recording_video_)
    {
        video_writer_->release();
        ui->zone_log_->append("\nTotal frame:"+QString::number(total_frame));
        recording_video_=false;
        video_index_++;
    }

    mat_original.release();
    mat_visual.release();



}

void MainWindow::ChangeTitle()
{
    QString name;
    name = QFileDialog::getOpenFileName(this,"Répertoir d'enregistrement");
    if(name=="")
        return;

    ui->line_edit_title_->setText(name);

    mat_title_ = imread(name.toStdString());
   // mat_title_ =OpencvTools::ChangeUniChannels( mat_title_);
}

void MainWindow::ChangeDirectoryIn()
{
    QString name;
    if(!ui->radio_in_video->isChecked())
        name = QFileDialog::getExistingDirectory(this,"Répertoir d'enregistrement");
    else
        name = QFileDialog::getOpenFileName(this,"Répertoir d'enregistrement");

    if(name=="")
        return;
    ui->line_edit_directory_in->setText(name);

    if(ui->radio_in_video->isChecked())
    {
        video_cap_->open(ui->line_edit_directory_in->text().toStdString());
        ui->zone_log_->append("\nOuverture de: "+ui->line_edit_directory_in->text());
        if(video_cap_->isOpened())
        {
            ui->zone_log_->append( "Ouverture reussi");
            fps_ = video_cap_->get(CV_CAP_PROP_FPS);
            ui->zone_log_->append("\n fps:"+QString::number(fps_));
            ui->spin_fps_in->setValue(fps_);
            ui->spin_fps_out->setValue(fps_);
        }
        else
            ui->zone_log_->append( "Erreur d\'ouverture");
    }
    else
    {

        QDir dir(ui->line_edit_directory_in->text());
        QStringList l= dir.entryList();


        image_path_ =dir.entryInfoList();
        ui->zone_log_->append("\nFile: ");
        for(int i=0;i<image_path_.length();i++)
            ui->zone_log_->append("\n"+image_path_.at(i).absoluteFilePath());
        image_ind_=0;
        ui->spin_fps_in->setValue(10);
    }
}

void MainWindow::ChangeDirectoryOut()
{
    QString name = QFileDialog::getExistingDirectory(this,"Répertoir d'enregistrement");

    if(name=="")
        return;
    ui->line_edit_directory_out->setText(name);

}

void MainWindow::Play()
{
    index_image_freq_=1;
    image_index_ = ui->spinBox_num_->value();

    if(ui->radio_in_video->isChecked())
    {
        if(!video_cap_->isOpened())
        {
            video_cap_->open(ui->line_edit_directory_in->text().toStdString());
            ui->zone_log_->append("\nOuverture de: "+ui->line_edit_directory_in->text());
            if(video_cap_->isOpened())
            {
                ui->zone_log_->append( "Ouverture reussi");
                fps_ = video_cap_->get(CV_CAP_PROP_FPS);
                ui->zone_log_->append("\n fps:"+QString::number(fps_));
                ui->spin_fps_in->setValue(fps_);
                ui->spin_fps_out->setValue(fps_);
            }
            else
                ui->zone_log_->append( "Erreur d\'ouverture");
        }
        if(video_cap_->isOpened())
        {
            timer_.start((int)(1000/fps_));
            ui->zone_log_->append("\n fps:"+QString::number(fps_));
        }
        else
        {
            ui->zone_log_->append( "\n close : Stop");
            Stop();
            return;
        }
    }
    else
    {
        if(image_path_.length()>0)
        {
            timer_.start((int)(1000/fps_));
            ui->zone_log_->append( "\n image start");

        }
        else
        {
            Stop();
            return;
        }
    }

    ui->button_stop->setChecked(false);
    ui->button_pause->setChecked(false);
    ui->button_play->setChecked(true);
    ui->dock_control->setEnabled(false);

}
void MainWindow::FpsIn()
{
    fps_= ui->spin_fps_in->value();
}

void MainWindow::Pause()
{
    timer_.stop();
    ui->zone_log_->append( "\npause");

    ui->button_stop->setChecked(false);
    ui->button_pause->setChecked(true);
    ui->button_play->setChecked(false);
    ui->dock_control->setEnabled(true);
}
void MainWindow::Stop()
{
    video_cap_->release();
    video_writer_->release();
    cvReleaseVideoWriter(&writer);
    timer_.stop();
    ui->zone_log_->append( "\nStop");

    if(video_cap_->isOpened())
        ui->zone_log_->append( "   video  :open ");
    else
        ui->zone_log_->append( "   video  :close ");


    image_index_=0;
    video_index_=0;
    image_ind_=0;
    ui->button_stop->setChecked(true);
    ui->button_pause->setChecked(false);
    ui->button_play->setChecked(false);
    ui->dock_control->setEnabled(true);

}


void MainWindow::Recording()
{

}

MainWindow::~MainWindow()
{
    delete ui;
}

QString NumToString(int num,int sh)
{
    QString str;
    int log_num;
    if(num==0)
        log_num =0;
    else
        log_num =log10(num);

    for(int i=1;i<(sh - log_num);i++)
    {
        str+="0";
    }
    str += QString::number(num);


    return str;
}

/**
 * Rotate an image
 */
void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
    int len = std::max(src.cols, src.rows);
    cv::Point2f pt(len/2., len/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

    cv::warpAffine(src, dst, r, cv::Size(len, len));
}

void RotateLeft(cv::Mat& src, cv::Mat& dst)
{
    for(int i=0,jj=src.cols-1;i<dst.rows&&jj>=0;i++,jj--)
        for(int j=0,ii=0;j<dst.cols&&ii<src.rows;j++,ii++)
        {
            for(int k=0;k<src.channels();k++)
                dst.data[(i*dst.cols+j)*dst.channels()+k]=src.data[(ii*src.cols+jj)*src.channels()+k];
        }
}

void RotateRigth(cv::Mat& src, cv::Mat& dst)
{
    for(int i=0,jj=0;i<dst.rows&&jj<src.cols;i++,jj++)
        for(int j=0,ii=src.rows-1;j<dst.cols&&ii>=0;j++,ii--)
        {
            for(int k=0;k<src.channels();k++)
                dst.data[(i*dst.cols+j)*dst.channels()+k]=src.data[(ii*src.cols+jj)*src.channels()+k];
        }
}

void RotateInv(cv::Mat& src, cv::Mat& dst)
{
    for(int i=0,ii=src.rows-1;i<src.rows;i++,ii--)
        for(int j=0,jj=src.cols-1;j<src.cols;j++,jj--)
        {
            for(int k=0;k<src.channels();k++)
                dst.data[(i*dst.cols+j)*dst.channels()+k]=src.data[(ii*src.cols+jj)*src.channels()+k];
        }
}

void ConvMono(cv::Mat& src, cv::Mat& dst)
{
    dst.create(src.rows,src.cols,CV_8UC1);
    for(int i=0;i<src.rows;i++)
        for(int j=0;j<src.cols;j++)
        {
                dst.data[i*dst.cols+j]=src.data[(i*src.cols+j)*src.channels()];
        }
}
