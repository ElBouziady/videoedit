﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QFileDialog>
#include <QDirIterator>
#include <QtCore/qmath.h>


#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

//#include "opencv_tools.h"


namespace Ui {
class MainWindow;
}

using namespace cv;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void ChangeTitle();

    void ChangeDirectoryIn();
    void ChangeDirectoryOut();
    void Recording();
    void Play();
    void Pause();
    void Stop();
    void StepImg();
    void FpsIn();
    
private:
    Ui::MainWindow *ui;
    Mat mat_image_;
    Mat mat_display_;
    Mat mat_title_;

    QFileInfoList image_path_;
    int image_ind_;
    VideoCapture *video_cap_;
    VideoWriter *video_writer_;
    CvVideoWriter* writer ;
    int total_frame;
    int frame_max_;

    QTimer timer_;
    double fps_;

    int image_index_;
    int video_index_;

    bool recording_video_;
    int index_image_freq_;

    int last_value;
    int new_value;
};

#endif // MAINWINDOW_H
